import HeroImage from './illustration.png'
import codepathLogo from './codepath.webp'
import railwayLogo from './railway.png'
import githubLogo from './github.png'
import Logo from './eduLogo.png'
import LogoNoBackground from './logo/png/logo-no-background.png'
import Indexquote from './dashboard.jpeg'
import CareerRoadmap from './career_roadmap.jpeg'
import Internship from './internship.jpeg'
import InterviewPrep from './interview_prep.jpeg'
import Notes from './notes.jpeg'
import Projects from './projects.jpeg'
import SkillsDevelopment from './skills_dev.jpeg'

export {
    HeroImage,
    Logo,
    codepathLogo,
    railwayLogo,
    githubLogo,
    LogoNoBackground,
    Indexquote,
    CareerRoadmap,
    Internship,
    InterviewPrep,
    Notes,
    Projects,
    SkillsDevelopment,
};