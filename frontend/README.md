# EduCompanio - Frontend

This is the frontend of the EduCompanio project. It is a React application that uses the [Material-UI](https://material-ui.com/) library for styling.
