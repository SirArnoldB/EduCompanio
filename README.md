# EduCompanio

Designed and developed by: Arnold Bhebhe and John Adeyemo

## About

### Description and Purpose

EduCompanio is an all-in-one platform designed to assist students in organizing, planning, and building their careers. It offers a comprehensive suite of features that cater to various aspects of a student's academic and professional journey. By integrating functionalities like tracking internships, preparing for interviews, and skill development, EduCompanio aims to streamline the process of career planning and advancement for students.

### Inspiration

Our inspiration comes from personal experiences as college students and the challenges faced by our peers. We observed that students often find it difficult to navigate through the job application process, establish a clear career path, access resources efficiently, and learn from past experiences. EduCompanio is our solution to these challenges - a one-stop platform for students to organize, plan, and build their careers effectively.

## Tech Stack

Frontend:

Backend:

## Features

### Landing Page ✅

On visiting EduCompanio, every user can access the landing page. The landing page shows the features of the app.

### Organizations

This feature allows students to add and track on-campus and off-campus organizations. Students can add a custom organization if it's not already listed on the platform.

### Internships ✅

Provides a customizable Trello-like board for students to track their internship applications, along with their respective statuses. Offers a curated list of websites for discovering internship opportunities.

### Interview Prep

This feature provides resources that students can use to prepare for interviews. It aims to make it easier for students across all disciplines to find necessary resources quickly and efficiently.

### Projects ✅

Provides a curated list of websites for discovering hackathons and open-source projects, catering to students interested in honing their technical skills.

### Notes ✅

Allows students to journal about their experiences and take notes, ensuring all valuable information is stored in one accessible location.

### Skills Development

Recommends websites and platforms for students to further develop their skills beyond the classroom environment, enhancing their professional profile.

### Career Roadmaps

This feature provides career roadmaps for students. These roadmaps give an overview of the skills and experiences needed to follow a certain career path.

## Installation Instructions

[instructions go here]
